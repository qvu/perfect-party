DROP TRIGGER IF EXISTS checkTimeConflict;
CREATE TRIGGER checkTimeConflict BEFORE INSERT ON event_bookings
BEGIN
    SELECT CASE 
        WHEN (
            EXISTS (
                SELECT * FROM event_bookings E
                WHERE venue_id = NEW.venue_id
            ) 
            AND
            EXISTS (
                WITH M AS (
                    SELECT start_time, 
                        DATETIME(start_time, '+' || (duration + recess_time) || ' hours') AS expected_end_time, 
                        recess_time
                    FROM event_bookings E
                    JOIN venues V
                    ON E.venue_id = V.id
                    WHERE venue_id = NEW.venue_id
                )
                SELECT start_time AS next_event_start_time, recess_time FROM (
                    SELECT *
                    FROM M 
                    WHERE expected_end_time > NEW.start_time
                    ORDER BY expected_end_time ASC 
                    LIMIT 1
                ) 
                WHERE DATETIME(NEW.start_time, '+' || (NEW.duration + recess_time) || ' hours') > next_event_start_time
            )
        ) 
        THEN RAISE(ABORT, 'There is a time conflict at the chosen venue') 
    END; 
END;


DROP TRIGGER IF EXISTS checkTimeConflict;
CREATE TRIGGER checkTimeConflict BEFORE UPDATE ON event_bookings
BEGIN
    SELECT CASE 
        WHEN (
            EXISTS (
                WITH M AS (
                    SELECT start_time, 
                        DATETIME(start_time, '+' || (duration + recess_time) || ' hours') AS expected_end_time, 
                        recess_time
                    FROM event_bookings E
                    JOIN venues V
                    ON E.venue_id = V.id
                    WHERE venue_id = NEW.venue_id
                    AND E.id <> NEW.id
                )
                SELECT start_time AS next_event_start_time, recess_time FROM (
                    SELECT *
                    FROM M 
                    WHERE expected_end_time > NEW.start_time
                    ORDER BY expected_end_time ASC 
                    LIMIT 1
                ) 
                WHERE DATETIME(NEW.start_time, '+' || (NEW.duration + recess_time) || ' hours') > next_event_start_time
            )
        ) 
        THEN RAISE(ABORT, 'There is a time conflict at the chosen venue') 
    END; 
END;