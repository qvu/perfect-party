class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.string :client_name
      t.integer :account_type_id
      t.string :street_number
      t.string :street_name
      t.string :city
      t.string :postal_code
      t.string :email
      t.string :phone_number

      t.timestamps
    end
  end
end
