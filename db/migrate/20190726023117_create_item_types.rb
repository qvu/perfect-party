class CreateItemTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :item_types do |t|
      t.string :item_type_name
      t.string :unit

      t.timestamps
    end
  end
end
