class CreateSuppliers < ActiveRecord::Migration[5.1]
  def change
    create_table :suppliers do |t|
      t.string :supplier_name
      t.string :street_number
      t.string :street_name
      t.string :city
      t.string :postal_code
      t.string :email
      t.string :phone_number

      t.timestamps
    end
  end
end
