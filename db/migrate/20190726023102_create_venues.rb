class CreateVenues < ActiveRecord::Migration[5.1]
  def change
    create_table :venues do |t|
      t.string :venue_name
      t.string :street_number
      t.string :street_name
      t.string :city
      t.string :postal_code
      t.integer :capacity
      t.decimal :recess_time
      t.decimal :pricing

      t.timestamps
    end
  end
end
