class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :item_name
      t.integer :item_type_id
      t.integer :supplier_id
      t.decimal :pricing

      t.timestamps
    end
  end
end
