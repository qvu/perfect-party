class CreateBillingInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :billing_infos do |t|
      t.integer :client_id
      t.string :card_number
      t.string :name_on_card
      t.string :security_code
      t.integer :month_expiry
      t.integer :year_expiry

      t.timestamps
    end
  end
end
