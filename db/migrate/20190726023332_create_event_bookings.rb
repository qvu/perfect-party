class CreateEventBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :event_bookings do |t|
      t.string :event_name
      t.integer :client_id
      t.decimal :budget
      t.integer :attendance
      t.integer :venue_id
      t.datetime :start_time
      t.decimal :duration
      t.boolean :is_public
      t.string :notes
      t.datetime :actual_end_time
      t.decimal :amount
      t.string :billing_card
      t.date :billing_date

      t.timestamps
    end
  end
end
