class CreateItemBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :item_bookings do |t|
      t.integer :event_id
      t.integer :item_id
      t.decimal :quantity
      t.string :notes

      t.timestamps
    end
  end
end
