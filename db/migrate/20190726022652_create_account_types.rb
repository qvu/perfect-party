class CreateAccountTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :account_types do |t|
      t.string :account_type_name

      t.timestamps
    end
  end
end
