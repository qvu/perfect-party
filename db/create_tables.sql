DROP TABLE IF EXISTS account_types;
CREATE TABLE "account_types" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"account_type_name" varchar(200) NOT NULL,
"created_at" datetime NOT NULL,
"updated_at" datetime NOT NULL
);

DROP TABLE IF EXISTS clients;
CREATE TABLE "clients" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"client_name" varchar(200) NOT NULL,
"account_type_id" int NOT NULL,
"street_number" varchar(50) NOT NULL,
"street_name" varchar(200) NOT NULL,
"city" varchar(50) NOT NULL,
"postal_code" varchar(10) NOT NULL,
"email" varchar(50) NOT NULL,
"phone_number" varchar(50) NOT NULL,
"created_at" datetime NOT NULL, 
"updated_at" datetime NOT NULL,
CONSTRAINT fk_client_account_type FOREIGN KEY ("account_type_id") REFERENCES account_types("id")
);

DROP TABLE IF EXISTS billing_infos;
CREATE TABLE "billing_infos" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"client_id" int NOT NULL,
"card_number" varchar(20) NOT NULL,
"name_on_card" varchar(100) NOT NULL,
"security_code" varchar(3) NOT NULL,
"month_expiry" int NOT NULL,
"year_expiry" int NOT NULL,
"created_at" datetime NOT NULL, 
"updated_at" datetime NOT NULL,
CONSTRAINT uq_billing_info_client_card UNIQUE ("client_id", "card_number"),
CONSTRAINT fk_billing_info_client FOREIGN KEY ("client_id") REFERENCES clients("id"),
CHECK(month_expiry<=12 and month_expiry>0),
CHECK(year_expiry>0)
);

DROP TABLE IF EXISTS venues;
CREATE TABLE "venues" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"venue_name" varchar(200) NOT NULL,
"street_number" varchar(50) NOT NULL,
"street_name" varchar(200) NOT NULL,
"city" varchar(50) NOT NULL,
"postal_code" varchar(10) NOT NULL,
"capacity" int NOT NULL,
"recess_time" decimal(2,2) NOT NULL,
"pricing" decimal(8,2) NOT NULL,
"created_at" datetime NOT NULL, 
"updated_at" datetime NOT NULL
);

DROP TABLE IF EXISTS item_types;
CREATE TABLE "item_types" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"item_type_name" varchar(200) NOT NULL,
"unit" varchar(50),
"created_at" datetime NOT NULL,
"updated_at" datetime NOT NULL
);

DROP TABLE IF EXISTS suppliers;
CREATE TABLE "suppliers" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"supplier_name" varchar(200) NOT NULL,
"street_number" varchar(50) NOT NULL,
"street_name" varchar(200) NOT NULL,
"city" varchar(50) NOT NULL,
"postal_code" varchar(10) NOT NULL,
"email" varchar(50) NOT NULL,
"phone_number" varchar(50) NOT NULL,
"created_at" datetime NOT NULL, 
"updated_at" datetime NOT NULL
);

DROP TABLE IF EXISTS items;
CREATE TABLE "items" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"item_name" varchar(200) NOT NULL,
"item_type_id" int NOT NULL,
"supplier_id" int NOT NULL,
"pricing" decimal(8,2),
"created_at" datetime NOT NULL, 
"updated_at" datetime NOT NULL,
CONSTRAINT fk_item_item_type FOREIGN KEY ("item_type_id") REFERENCES item_types("id"),
CONSTRAINT fk_item_supplier FOREIGN KEY ("supplier_id") REFERENCES suppliers("id")
);

DROP TABLE IF EXISTS event_bookings;
CREATE TABLE "event_bookings" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"event_name" varchar(200) NOT NULL,
"client_id" int NOT NULL,
"budget" decimal(8,2),
"attendance" int NOT NULL,
"venue_id" int NOT NULL,
"start_time" datetime NOT NULL,
"duration" decimal(2,2) NOT NULL,
"is_public" bool NOT NULL,
"notes" varchar(500),
"actual_end_time" datetime,
"amount" decimal(8,2),
"billing_card" int NOT NULL,
"billing_date" date,
"created_at" datetime NOT NULL, 
"updated_at" datetime NOT NULL,
CONSTRAINT fk_event_booking_client FOREIGN KEY ("client_id") REFERENCES clients("id"),
CONSTRAINT fk_event_booking_venue FOREIGN KEY ("venue_id") REFERENCES venues("id"),
CONSTRAINT fk_event_booking_billing_info FOREIGN KEY ("billing_card") REFERENCES billing_infos("id")
CONSTRAINT ck_event_booking_endtime CHECK(actual_end_time > start_time)
);

DROP TABLE IF EXISTS item_bookings;
CREATE TABLE "item_bookings" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"event_id" int NOT NULL,
"item_id" int NOT NULL,
"quantity" decimal(8,2) NOT NULL,
"notes" varchar(500),
"created_at" datetime NOT NULL, 
"updated_at" datetime NOT NULL,
CONSTRAINT uq_item_booking_event_item UNIQUE ("event_id", "item_id"),
CONSTRAINT fk_item_booking_event FOREIGN KEY ("event_id") REFERENCES event_bookings("id"),
CONSTRAINT fk_item_booking_item FOREIGN KEY ("item_id") REFERENCES items("id")
);

