# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190726023354) do

  create_table "account_types", force: :cascade do |t|
    t.string "account_type_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "billing_infos", force: :cascade do |t|
    t.integer "client_id"
    t.string "card_number"
    t.string "name_on_card"
    t.string "security_code"
    t.integer "month_expiry"
    t.integer "year_expiry"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string "client_name"
    t.integer "account_type_id"
    t.string "street_number"
    t.string "street_name"
    t.string "city"
    t.string "postal_code"
    t.string "email"
    t.string "phone_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "event_bookings", force: :cascade do |t|
    t.string "event_name"
    t.integer "client_id"
    t.decimal "budget"
    t.integer "attendance"
    t.integer "venue_id"
    t.datetime "start_time"
    t.decimal "duration"
    t.boolean "is_public"
    t.string "notes"
    t.datetime "actual_end_time"
    t.decimal "amount"
    t.string "billing_card"
    t.date "billing_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "item_bookings", force: :cascade do |t|
    t.integer "event_id"
    t.integer "item_id"
    t.decimal "quantity"
    t.string "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "item_types", force: :cascade do |t|
    t.string "item_type_name"
    t.string "unit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", force: :cascade do |t|
    t.string "item_name"
    t.integer "item_type_id"
    t.integer "supplier_id"
    t.decimal "pricing"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "suppliers", force: :cascade do |t|
    t.string "supplier_name"
    t.string "street_number"
    t.string "street_name"
    t.string "city"
    t.string "postal_code"
    t.string "email"
    t.string "phone_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "venues", force: :cascade do |t|
    t.string "venue_name"
    t.string "street_number"
    t.string "street_name"
    t.string "city"
    t.string "postal_code"
    t.integer "capacity"
    t.decimal "recess_time"
    t.decimal "pricing"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
