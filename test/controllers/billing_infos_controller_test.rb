require 'test_helper'

class BillingInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @billing_info = billing_infos(:one)
  end

  test "should get index" do
    get billing_infos_url
    assert_response :success
  end

  test "should get new" do
    get new_billing_info_url
    assert_response :success
  end

  test "should create billing_info" do
    assert_difference('BillingInfo.count') do
      post billing_infos_url, params: { billing_info: { card_number: @billing_info.card_number, client_id: @billing_info.client_id, month_expiry: @billing_info.month_expiry, name_on_card: @billing_info.name_on_card, security_code: @billing_info.security_code, year_expiry: @billing_info.year_expiry } }
    end

    assert_redirected_to billing_info_url(BillingInfo.last)
  end

  test "should show billing_info" do
    get billing_info_url(@billing_info)
    assert_response :success
  end

  test "should get edit" do
    get edit_billing_info_url(@billing_info)
    assert_response :success
  end

  test "should update billing_info" do
    patch billing_info_url(@billing_info), params: { billing_info: { card_number: @billing_info.card_number, client_id: @billing_info.client_id, month_expiry: @billing_info.month_expiry, name_on_card: @billing_info.name_on_card, security_code: @billing_info.security_code, year_expiry: @billing_info.year_expiry } }
    assert_redirected_to billing_info_url(@billing_info)
  end

  test "should destroy billing_info" do
    assert_difference('BillingInfo.count', -1) do
      delete billing_info_url(@billing_info)
    end

    assert_redirected_to billing_infos_url
  end
end
