require 'test_helper'

class EventBookingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @event_booking = event_bookings(:one)
  end

  test "should get index" do
    get event_bookings_url
    assert_response :success
  end

  test "should get new" do
    get new_event_booking_url
    assert_response :success
  end

  test "should create event_booking" do
    assert_difference('EventBooking.count') do
      post event_bookings_url, params: { event_booking: { actual_end_time: @event_booking.actual_end_time, amount: @event_booking.amount, attendance: @event_booking.attendance, billing_card: @event_booking.billing_card, billing_date: @event_booking.billing_date, budget: @event_booking.budget, client_id: @event_booking.client_id, duration: @event_booking.duration, event_name: @event_booking.event_name, is_public: @event_booking.is_public, notes: @event_booking.notes, start_time: @event_booking.start_time, venue_id: @event_booking.venue_id } }
    end

    assert_redirected_to event_booking_url(EventBooking.last)
  end

  test "should show event_booking" do
    get event_booking_url(@event_booking)
    assert_response :success
  end

  test "should get edit" do
    get edit_event_booking_url(@event_booking)
    assert_response :success
  end

  test "should update event_booking" do
    patch event_booking_url(@event_booking), params: { event_booking: { actual_end_time: @event_booking.actual_end_time, amount: @event_booking.amount, attendance: @event_booking.attendance, billing_card: @event_booking.billing_card, billing_date: @event_booking.billing_date, budget: @event_booking.budget, client_id: @event_booking.client_id, duration: @event_booking.duration, event_name: @event_booking.event_name, is_public: @event_booking.is_public, notes: @event_booking.notes, start_time: @event_booking.start_time, venue_id: @event_booking.venue_id } }
    assert_redirected_to event_booking_url(@event_booking)
  end

  test "should destroy event_booking" do
    assert_difference('EventBooking.count', -1) do
      delete event_booking_url(@event_booking)
    end

    assert_redirected_to event_bookings_url
  end
end
