require 'test_helper'

class ItemBookingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @item_booking = item_bookings(:one)
  end

  test "should get index" do
    get item_bookings_url
    assert_response :success
  end

  test "should get new" do
    get new_item_booking_url
    assert_response :success
  end

  test "should create item_booking" do
    assert_difference('ItemBooking.count') do
      post item_bookings_url, params: { item_booking: { event_id: @item_booking.event_id, item_id: @item_booking.item_id, notes: @item_booking.notes, quantity: @item_booking.quantity } }
    end

    assert_redirected_to item_booking_url(ItemBooking.last)
  end

  test "should show item_booking" do
    get item_booking_url(@item_booking)
    assert_response :success
  end

  test "should get edit" do
    get edit_item_booking_url(@item_booking)
    assert_response :success
  end

  test "should update item_booking" do
    patch item_booking_url(@item_booking), params: { item_booking: { event_id: @item_booking.event_id, item_id: @item_booking.item_id, notes: @item_booking.notes, quantity: @item_booking.quantity } }
    assert_redirected_to item_booking_url(@item_booking)
  end

  test "should destroy item_booking" do
    assert_difference('ItemBooking.count', -1) do
      delete item_booking_url(@item_booking)
    end

    assert_redirected_to item_bookings_url
  end
end
