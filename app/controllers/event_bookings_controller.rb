class EventBookingsController < ApplicationController
  before_action :set_event_booking, only: [:show, :edit, :update, :destroy]

  # GET /event_bookings
  # GET /event_bookings.json
  def index
    @event_bookings = EventBooking.all
    @clients = Client.all
    @venues = Venue.all
    @billing_infos = BillingInfo.all
  end

  # GET /event_bookings/1
  # GET /event_bookings/1.json
  def show
  end

  # GET /event_bookings/new
  def new
    @event_booking = EventBooking.new
    @clients = Client.all
    @venues = Venue.all
    @billingCards = BillingInfo.all
  end

  # GET /event_bookings/1/edit
  def edit
    @clients = Client.all
    @venues = Venue.all
    @billingCards = BillingInfo.all
  end

  # POST /event_bookings
  # POST /event_bookings.json
  def create
    @event_booking = EventBooking.new(event_booking_params)

    respond_to do |format|
      begin
        result = @event_booking.save
      rescue => e
        format.html { redirect_to @event_booking, notice: 'New entry could not be created due to invalid input or constraint violation.' }
        format.json { render :show, status: :unprocessable_entity, location: @event_booking }
      end

      if result
        format.html { redirect_to @event_booking, notice: 'Event booking was successfully created.' }
        format.json { render :show, status: :created, location: @event_booking }
      else
        format.html { render :new }
        format.json { render json: @event_booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /event_bookings/1
  # PATCH/PUT /event_bookings/1.json
  def update
    respond_to do |format|
      begin
        result = @event_booking.save
      rescue => e
        format.html { redirect_to @event_booking, notice: 'The change you wanted was rejected due to invalid input or constraint violation.' }
        format.json { render :show, status: :unprocessable_entity, location: @event_booking }
      end

      if result
        format.html { redirect_to @event_booking, notice: 'Event booking was successfully updated.' }
        format.json { render :show, status: :ok, location: @event_booking }
      else
        format.html { render :edit }
        format.json { render json: @event_booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /event_bookings/1
  # DELETE /event_bookings/1.json
  def destroy
    @event_booking.destroy
    respond_to do |format|
      format.html { redirect_to event_bookings_url, notice: 'Event booking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event_booking
      @event_booking = EventBooking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_booking_params
      params.require(:event_booking).permit(:event_name, :client_id, :budget, :attendance, :venue_id, :start_time, :duration, :is_public, :notes, :actual_end_time, :amount, :billing_card, :billing_date)
    end
end
