class BillingInfosController < ApplicationController
  before_action :set_billing_info, only: [:show, :edit, :update, :destroy]

  # GET /billing_infos
  # GET /billing_infos.json
  def index
    @billing_infos = BillingInfo.all
    @clients = Client.all
  end

  # GET /billing_infos/1
  # GET /billing_infos/1.json
  def show
  end

  # GET /billing_infos/new
  def new
    @billing_info = BillingInfo.new
    @clients = Client.all
  end

  # GET /billing_infos/1/edit
  def edit
    @clients = Client.all
  end

  # POST /billing_infos
  # POST /billing_infos.json
  def create
    @billing_info = BillingInfo.new(billing_info_params)

    respond_to do |format|
      begin
        result = @billing_info.save
      rescue => e
        format.html { redirect_to @billing_info, notice: 'New entry could not be created due to invalid input or constraint violation.' }
        format.json { render :show, status: :unprocessable_entity, location: @billing_info }
      end

      if result
        format.html { redirect_to @billing_info, notice: 'Billing info was successfully created.' }
        format.json { render :show, status: :created, location: @billing_info }
      else
        format.html { render :new }
        format.json { render json: @billing_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /billing_infos/1
  # PATCH/PUT /billing_infos/1.json
  def update
    respond_to do |format|
      begin
        result = @billing_info.update(billing_info_params)
      rescue => e
        format.html { redirect_to @billing_info, notice: 'The change you wanted was rejected due to invalid input or constraint violation.' }
        format.json { render :show, status: :unprocessable_entity, location: @billing_info }
      end

      if result
        format.html { redirect_to @billing_info, notice: 'Billing info was successfully updated.' }
        format.json { render :show, status: :ok, location: @billing_info }
      else
        format.html { render :edit }
        format.json { render json: @billing_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /billing_infos/1
  # DELETE /billing_infos/1.json
  def destroy
    @billing_info.destroy
    respond_to do |format|
      format.html { redirect_to billing_infos_url, notice: 'Billing info was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_billing_info
      @billing_info = BillingInfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def billing_info_params
      params.require(:billing_info).permit(:client_id, :card_number, :name_on_card, :security_code, :month_expiry, :year_expiry)
    end
end
