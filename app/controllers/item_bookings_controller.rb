class ItemBookingsController < ApplicationController
  before_action :set_item_booking, only: [:show, :edit, :update, :destroy]

  # GET /item_bookings
  # GET /item_bookings.json
  def index
    @item_bookings = ItemBooking.all
    @items = Item.all
    @events = EventBooking.all
  end

  # GET /item_bookings/1
  # GET /item_bookings/1.json
  def show
  end

  # GET /item_bookings/new
  def new
    @item_booking = ItemBooking.new
    @events = EventBooking.all
    @items = Item.all
  end

  # GET /item_bookings/1/edit
  def edit
    @events = EventBooking.all
    @items = Item.all
  end

  # POST /item_bookings
  # POST /item_bookings.json
  def create
    @item_booking = ItemBooking.new(item_booking_params)

    respond_to do |format|
      begin
        result = @item_booking.save
      rescue => e
        format.html { redirect_to @item_booking, notice: 'New entry could not be created due to invalid input or constraint violation.' }
        format.json { render :show, status: :unprocessable_entity, location: @item_booking }
      end

      if 
        format.html { redirect_to @item_booking, notice: 'Item booking was successfully created.' }
        format.json { render :show, status: :created, location: @item_booking }
      else
        format.html { render :new }
        format.json { render json: @item_booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /item_bookings/1
  # PATCH/PUT /item_bookings/1.json
  def update
    respond_to do |format|
      begin
        result = @item_booking.update(item_booking_params)
      rescue => e
        format.html { redirect_to @item_booking, notice: 'The change you wanted was rejected due to invalid input or constraint violation.' }
        format.json { render :show, status: :unprocessable_entity, location: @item_booking }
      end

      if result
        format.html { redirect_to @item_booking, notice: 'Item booking was successfully updated.' }
        format.json { render :show, status: :ok, location: @item_booking }
      else
        format.html { render :edit }
        format.json { render json: @item_booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /item_bookings/1
  # DELETE /item_bookings/1.json
  def destroy
    @item_booking.destroy
    respond_to do |format|
      format.html { redirect_to item_bookings_url, notice: 'Item booking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item_booking
      @item_booking = ItemBooking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_booking_params
      params.require(:item_booking).permit(:event_id, :item_id, :quantity, :notes)
    end
end
