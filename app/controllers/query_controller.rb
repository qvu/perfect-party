class QueryController < ApplicationController

  # GET /query
  # GET /query.json
  def index
  end

  # GET /getQueryOne
  # GET /getQueryOne.json
  def getQueryOne
    requiredCapacity = params[:capacity]
    result = ActiveRecord::Base.connection.execute("
      SELECT event_name, venue_name, capacity FROM event_bookings E
      JOIN venues V
      ON E.venue_id = V.id
      WHERE E.venue_id IN 
        (SELECT id FROM venues WHERE capacity >= " + requiredCapacity +")")

    respond_to do |format|
      format.json  { render :json => result }
    end
  end

  # GET /getQueryTwo
  # GET /getQueryTwo.json
  def getQueryTwo
    venueId = params[:venueId]
    minBooking = params[:minBooking]
    result = ActiveRecord::Base.connection.execute("
      SELECT client_id, client_name FROM event_bookings E
      JOIN clients C
      ON E.client_id = C.id
      WHERE venue_id = " + venueId + 
      " GROUP BY client_id HAVING COUNT(*) >= " + minBooking)

    respond_to do |format|
      format.json  { render :json => result }
    end
  end

  # GET /getQueryThree
  # GET /getQueryThree.json
  def getQueryThree
    eventId = params[:eventId]
    result = ActiveRecord::Base.connection.execute("
      SELECT item_name, unit, pricing, supplier_name
      FROM item_bookings B
      JOIN items I
      ON B.item_id = I.id
      JOIN suppliers S
      ON I.supplier_id = S.id
      JOIN item_types T
      ON I.item_type_id  = T.id
      WHERE event_id = " + eventId)

    respond_to do |format|
      format.json  { render :json => result }
    end
  end

  # GET /getQueryFour
  # GET /getQueryFour.json
  def getQueryFour
    ceilingPrice = params[:ceilingPrice]
    result = ActiveRecord::Base.connection.execute("
      SELECT item_name, unit, pricing, quantity, supplier_name, event_name 
      FROM (
        SELECT I.id AS item_id, item_name, unit, pricing, supplier_name
        FROM items I
        INNER JOIN suppliers S
        ON I.supplier_id = S.id
        INNER JOIN item_types T
        ON I.item_type_id = T.id
        WHERE pricing <= " + ceilingPrice + "
      ) AS M
      INNER JOIN item_bookings B
      ON M.item_id = B.item_id
      JOIN event_bookings E
      On B.event_id = E.id")

    respond_to do |format|
      format.json  { render :json => result }
    end
  end

  # GET /getQueryFive
  # GET /getQueryFive.json
  def getQueryFive
    venueId = params[:venueId]
    result = ActiveRecord::Base.connection.execute("
      SELECT client_name, AVG(budget) AS avg_budget
      FROM event_bookings E
      LEFT OUTER JOIN clients C
      ON E.client_id = C.id
      WHERE venue_id = " + venueId + "
      GROUP BY client_id")

    respond_to do |format|
      format.json  { render :json => result }
    end
  end

  # GET /getQuerySix
  # GET /getQuerySix.json
  def getQuerySix
    itemTypeId = params[:itemTypeId]
    result = ActiveRecord::Base.connection.execute("
      SELECT supplier_name, item_name
      FROM suppliers S
      LEFT OUTER JOIN (
        SELECT * FROM items WHERE item_type_id = " + itemTypeId + "
      ) AS I
      ON S.id = I.supplier_id")

    respond_to do |format|
      format.json  { render :json => result }
    end
  end

end
