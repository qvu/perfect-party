json.extract! venue, :id, :venue_name, :street_number, :street_name, :city, :postal_code, :capacity, :recess_time, :pricing, :created_at, :updated_at
json.url venue_url(venue, format: :json)
