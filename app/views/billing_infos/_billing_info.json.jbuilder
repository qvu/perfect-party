json.extract! billing_info, :id, :client_id, :card_number, :name_on_card, :security_code, :month_expiry, :year_expiry, :created_at, :updated_at
json.url billing_info_url(billing_info, format: :json)
