json.extract! supplier, :id, :supplier_name, :street_number, :street_name, :city, :postal_code, :email, :phone_number, :created_at, :updated_at
json.url supplier_url(supplier, format: :json)
