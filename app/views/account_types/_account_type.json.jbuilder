json.extract! account_type, :id, :account_type_name, :created_at, :updated_at
json.url account_type_url(account_type, format: :json)
