json.extract! item_type, :id, :item_type_name, :unit, :created_at, :updated_at
json.url item_type_url(item_type, format: :json)
