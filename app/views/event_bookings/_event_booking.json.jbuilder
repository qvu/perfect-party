json.extract! event_booking, :id, :event_name, :client_id, :budget, :attendance, :venue_id, :start_time, :duration, :is_public, :notes, :actual_end_time, :amount, :billing_card, :billing_date, :created_at, :updated_at
json.url event_booking_url(event_booking, format: :json)
