json.extract! item_booking, :id, :event_id, :item_id, :quantity, :notes, :created_at, :updated_at
json.url item_booking_url(item_booking, format: :json)
