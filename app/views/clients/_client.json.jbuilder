json.extract! client, :id, :client_name, :account_type_id, :street_number, :street_name, :city, :postal_code, :email, :phone_number, :created_at, :updated_at
json.url client_url(client, format: :json)
