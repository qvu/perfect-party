json.extract! item, :id, :item_name, :item_type_id, :supplier_id, :pricing, :created_at, :updated_at
json.url item_url(item, format: :json)
