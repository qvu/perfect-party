Rails.application.routes.draw do
  get 'welcome/homepage'
  root 'welcome#homepage'
  
  resources :item_bookings
  resources :event_bookings
  resources :items
  resources :suppliers
  resources :item_types
  resources :venues
  resources :billing_infos
  resources :clients
  resources :account_types

  match '/query', to: 'query#index', via: 'get'
  match '/query/queryOne', to: 'query#queryOne', via: 'get'
  match '/query/getQueryOne', to: 'query#getQueryOne', via: 'get'
  match '/query/queryTwo', to: 'query#queryTwo', via: 'get'
  match '/query/getQueryTwo', to: 'query#getQueryTwo', via: 'get'
  match '/query/queryThree', to: 'query#queryThree', via: 'get'
  match '/query/getQueryThree', to: 'query#getQueryThree', via: 'get'
  match '/query/queryFour', to: 'query#queryFour', via: 'get'
  match '/query/getQueryFour', to: 'query#getQueryFour', via: 'get'
  match '/query/queryFive', to: 'query#queryFive', via: 'get'
  match '/query/getQueryFive', to: 'query#getQueryFive', via: 'get'
  match '/query/querySix', to: 'query#querySix', via: 'get'
  match '/query/getQuerySix', to: 'query#getQuerySix', via: 'get'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
